#!/usr/bin/env python3
import socket
import multiprocessing
from socket import error as socktererror
import errno


class PixelThread(multiprocessing.Process):
    def __init__(self, pixels, target, perpacket=1):
        multiprocessing.Process.__init__(self)
        self.pixels = pixels
        self.target = target
        self.perpacket = perpacket
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.sendbuf = ""
        self.bufcnt = 1

    def pixel(self, px):
        self.sendbuf += 'PX {} {} {:02x}{:02x}{:02x}\n'.format(px[0], px[1], px[2], px[3], px[4])
        if self.bufcnt == self.perpacket:
            self.sock.send(self.sendbuf.encode())
            self.bufcnt = 1
            self.sendbuf = ''
        else:
            self.bufcnt += 1

    def run(self):
        self.sock.connect(self.target)
        while True:
            for px in self.pixels:
                try:
                    self.pixel(px)
                except socktererror as e:
                    if e.errno != errno.ECONNRESET:
                        raise  # Not error we are looking for
                    self.sock.close()
                    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.sock.connect(self.target)
                    print("  reconnecting")


class pixelflut:
    def __init__(self, target, threads, perpacket, pixels):
        self.target = target
        self.threads = threads
        self.perpacket = perpacket
        self.pixels = pixels
        self.my_threads = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        for t in self.my_threads:
            t.join();

    def run(self):
        buf = list()
        for i in range(self.threads):
            buf.append(list())

        # Assigning pixels to Threads
        for i in range(len(self.pixels)):
            buf[i % self.threads].append(self.pixels[i])

        print("Spawning threads")
        for i in range(self.threads):
            thread = PixelThread(buf[i], self.target, self.perpacket)
            self.my_threads.append(thread)
            print(" -  thread {}".format(i))
            thread.start()

        for t in self.my_threads:
            t.join()
