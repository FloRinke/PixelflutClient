import socket
from PIL import Image
import threading
from socket import error as socktererror
import errno
from random import shuffle
from math import floor

PIC = 'gentoo.png'

SIZE_X = 1920
SIZE_Y = 1080

POSX = 600
POSY = 200


THREADS = 32
PXATONCE = 10

HOST = 'pixelflut.selfnet.de'
PORT = 1234


def readimg(filename):
    pxl = list()
    im = Image.open(filename) .convert('RGBA')
    _, _, wi, he = im.getbbox()
    for x in range(wi):
        for y in range(he):
            r, g, b, a = im.getpixel((x, y))
            if a == 255:
                # print("{} {} {} {}".format(r,g,b,a))
                # pixel(x,y,r,g,b)
                pxl.append((x, y, r, g, b))
    return pxl, wi, he


def rect(wi, he, r, g, b):
    pxl = list()
    for x in range(wi):
        for y in range(he):
            pxl.append((x, y, r, g, b))
    return pxl


class SenderThread(threading.Thread):
    def __init__(self, pxl):
        threading.Thread.__init__(self)
        self.pxl = pxl
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.sendbuf = ""
        self.bufcnt = 1

    def pixel(self, px):
        self.sendbuf += 'PX {} {} {:02x}{:02x}{:02x}\n'.format(px[0], px[1], px[2], px[3], px[4])
        self.bufcnt += 1
        #print(px)
        #print(self.sendbuf)
        if self.bufcnt >= PXATONCE:
            self.sock.send(self.sendbuf.encode())
            self.bufcnt = 1
            self.sendbuf = ''

    def run(self):
        self.sock.connect((HOST, PORT))
        while True:
            try:
                for px in self.pxl:
                    #print(px)
                    self.pixel(px)
            except socktererror as e:
                if e.errno != errno.ECONNRESET:
                    raise  # Not error we are looking for
                self.sock.close()
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.sock.connect((HOST, PORT))
                print("reconnecting")

if __name__ == '__main__':
    print("Rendering image")
    pixels, w, h = readimg(PIC)
    #w = 100
    #h = 100
    #pixels = rect(w, h, 255, 255, 255)

    print("{} {} {}".format(w, h, len(pixels)))

    if POSX is None:
        xoff = SIZE_X/2 - w/2
    else:
        xoff = POSX
    if POSY is None:
        yoff = SIZE_Y/2 - h/2
    else:
        yoff = POSY

    buf = list()
    for i in range(THREADS):
        buf.append(list())

    print("Applying offset")
    px2 = list()
    for px in pixels:
        px2.append((floor(px[0]+xoff), floor(px[1]+yoff), px[2], px[3], px[4]))

    print("Assigning pixels to Threads")
    for i in range(len(pixels)):
        buf[i % THREADS].append(px2[i])

    print("Assigning pixels to Threads")
    for i in range(THREADS):
        shuffle(buf[i])

    print("Spawning threads")
    my_threads = []
    for i in range(THREADS):
        thread = SenderThread(buf[i])
        my_threads.append(thread)
        print(" -  thread {}".format(i))
        thread.start()

    for t in my_threads:
        t.join()
