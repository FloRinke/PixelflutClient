#!/usr/bin/env python3
from PIL import Image
import argparse
from math import floor
from random import shuffle

import client

#HOST = 'pixelflut.selfnet.de'
#PORT = 1234
#THREADS = 16
#PERPACKET = 10

def readimg(filename):
    pxl = list()
    im = Image.open(filename) .convert('RGBA')
    _, _, wi, he = im.getbbox()
    for x in range(wi):
        for y in range(he):
            r, g, b, a = im.getpixel((x, y))
            if a == 255:
                # print("{} {} {} {}".format(r,g,b,a))
                # pixel(x,y,r,g,b)
                pxl.append((x, y, r, g, b))
    return pxl, wi, he


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Send imagefile to Pixelflut server')
    parser.add_argument('host', help='Target host')
    parser.add_argument('--threads', type=int, metavar='N', default=1, help='run N parallel threads')
    parser.add_argument('--size', type=int, metavar='N', default=1, help='send N Pixels per Package')
    parser.add_argument('--port', type=int, metavar='p', default=1234, help='send N Pixels per Package')
    parser.add_argument('-x', type=int, metavar='N', default=0, help='Offset on X axis')
    parser.add_argument('-y', type=int, metavar='N', default=0, help='Offset on Y axis')
    parser.add_argument('image', help='path to imagefile')

    args = parser.parse_args()

    print("Rendering image")
    rawpixels, w, h = readimg(args.image)
    print("  Size: {}x{}, {} effective Pixels".format(w, h, len(rawpixels)))

    print("Applying offset")
    pixels = list()
    for px in rawpixels:
        pixels.append((floor(px[0]+args.x), floor(px[1]+args.y), px[2], px[3], px[4]))

    print("Shuffling data order")
    shuffle(pixels)

    c = client.pixelflut((args.host, args.port), args.threads, args.size, pixels)
    c.run()
